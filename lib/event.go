package lib

// Generic type for events.
// Event describes to which action it relates,
// and associated data with it
type Event struct {
	Action     string      `json:"action"`
	TrustToken string      `json:"trustToken"`
	Payload    interface{} `json:"payload"`
}
