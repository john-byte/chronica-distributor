package lib

type Signal interface{}

type SignalNodeConnected struct {
	Uid string
}

type SignalNewEvent struct {
	FromUid string
	Event   Event
}

type SignalNewRootEvent struct {
	Event Event
}

type SignalNewUnicastEvent struct {
	FromUid string
	ToUid   string
	Event   Event
}

type SignalNewUnicastRootEvent struct {
	ToUid string
	Event Event
}
