package lib

import (
	"fmt"
	"path"
	"sync"
)

// - Uid: unique descriptive identifier of node
// - Node: node
type FiberParams struct {
	Uid  string
	Node Node
}

type FiberWithId struct {
	id    uint32
	fiber *Fiber
}

// An object to distribute events to fibers
type FiberDirector struct {
	externalSigChan chan Signal
	closeSig        chan struct{}
	fibersLock      *sync.Mutex
	fibers          map[string]*FiberWithId
	fiberIdCounter  uint32
	eventsCapaciry  uint32
	packetCapacity  uint32
	trustToken      string
	filePath        string
}

// Creates fiber orchester
// - fiberParams: nodes parameters
// - eventsCapacity: maximum number of event buckets
// - packetCapacity: number of events in bucket
func MakeFiberDirector(
	fibersParams []FiberParams,
	eventsCapacity uint32,
	packetCapacity uint32,
	trustToken string,
	filePath string,
) (*FiberDirector, error) {
	fibers := make(map[string]*FiberWithId, 0)
	var idCounter uint32 = 0

	for _, param := range fibersParams {
		idCounter++

		fiberPath := ""
		if len(filePath) > 0 {
			fiberPath = path.Join(
				filePath,
				fmt.Sprintf(
					"fiber_%v.json",
					param.Uid,
				),
			)
		}
		fib, err := MakeFiber(param.Node, eventsCapacity, packetCapacity, fiberPath)
		if err != nil {
			return nil, err
		}

		fibers[param.Uid] = &FiberWithId{
			id:    idCounter,
			fiber: fib,
		}
	}

	res := FiberDirector{
		externalSigChan: make(chan Signal, 1024),
		closeSig:        make(chan struct{}, 2),
		fibersLock:      &sync.Mutex{},
		fibers:          fibers,
		fiberIdCounter:  idCounter,
		eventsCapaciry:  eventsCapacity,
		packetCapacity:  packetCapacity,
		trustToken:      trustToken,
		filePath:        filePath,
	}

	return &res, nil
}

// Starts fiber orchester
func (self *FiberDirector) Run() {
	for _, fib := range self.fibers {
		fib.fiber.Run()
	}

	go func() {
		for {
			var signal Signal
			select {
			case <-self.closeSig:
				return
			case signal = <-self.externalSigChan:
			}

			if v, ok := signal.(SignalNodeConnected); ok {
				self.fibersLock.Lock()
				fiber, fiberExists := self.fibers[v.Uid]
				if fiberExists {
					fiber.fiber.AckNodeConnected()
				} else {
					fmt.Printf("[CHRONICA] [FIB-MANAGER] Err => Node with uid %v doesn't exist\n", v.Uid)
				}
				self.fibersLock.Unlock()
				continue
			}

			if v, ok := signal.(SignalNewEvent); ok {
				self.fibersLock.Lock()
				senderFiber, senderExists := self.fibers[v.FromUid]
				if senderExists {
					for _, recvFib := range self.fibers {
						if recvFib.id != senderFiber.id {
							recvFib.fiber.SendEvent(v.Event)
						}
					}
				} else {
					fmt.Printf("[CHRONICA] [FIB-MANAGER] Err => Sender node with uid %v doesn't exist\n", v.FromUid)
				}
				self.fibersLock.Unlock()
				continue
			}

			if v, ok := signal.(SignalNewRootEvent); ok {
				self.fibersLock.Lock()
				for _, recvFib := range self.fibers {
					recvFib.fiber.SendRootEvent(v.Event)
				}
				self.fibersLock.Unlock()
				continue
			}

			if v, ok := signal.(SignalNewUnicastEvent); ok {
				self.fibersLock.Lock()
				senderFiber, senderExists := self.fibers[v.FromUid]
				receiverFiber, receiverExists := self.fibers[v.ToUid]

				if senderExists && receiverExists {
					if senderFiber.id != receiverFiber.id {
						receiverFiber.fiber.SendEvent(v.Event)
					}
				} else if !senderExists {
					fmt.Printf("[CHRONICA] [FIB-MANAGER] Err => Sender node with uid %v doesn't exist\n", v.FromUid)
				} else {
					fmt.Printf("[CHRONICA] [FIB-MANAGER] Err => Receiver node with uid %v doesn't exist\n", v.ToUid)
				}

				self.fibersLock.Unlock()
				continue
			}

			if v, ok := signal.(SignalNewUnicastRootEvent); ok {
				self.fibersLock.Lock()
				receiverFiber, receiverExists := self.fibers[v.ToUid]
				if receiverExists {
					receiverFiber.fiber.SendRootEvent(v.Event)
				} else {
					fmt.Printf("[CHRONICA] [FIB-MANAGER] Err => Receiver node with uid %v doesn't exist\n", v.ToUid)
				}
				self.fibersLock.Unlock()
				continue
			}
		}
	}()
}

// Stops fiber orchester
func (self *FiberDirector) Unrun() {
	self.closeSig <- struct{}{}
	close(self.closeSig)
	close(self.externalSigChan)
}

// Dispatches signal to fiber orchester
// - sig: signal object
func (self *FiberDirector) SendSignal(sig Signal) {
	self.externalSigChan <- sig
}

func (self *FiberDirector) RegisterNode(fiberParams FiberParams) error {
	fiberPath := ""
	if len(self.filePath) > 0 {
		fiberPath = path.Join(
			self.filePath,
			fmt.Sprintf(
				"fiber_%v.json",
				fiberParams.Uid,
			),
		)
	}

	fiber, err := MakeFiber(
		fiberParams.Node,
		self.eventsCapaciry,
		self.packetCapacity,
		fiberPath,
	)
	if err != nil {
		return err
	}

	self.fiberIdCounter++
	self.fibersLock.Lock()
	self.fibers[fiberParams.Uid] = &FiberWithId{
		id:    self.fiberIdCounter,
		fiber: fiber,
	}
	self.fibersLock.Unlock()
	fiber.Run()

	go func() {
		uids := make([]string, 0)
		self.fibersLock.Lock()
		for uid := range self.fibers {
			uids = append(uids, uid)
		}
		self.fibersLock.Unlock()

		event := Event{
			Action:     InternalEventPullCurrentState,
			TrustToken: self.trustToken,
			Payload:    uids,
		}
		self.externalSigChan <- SignalNewUnicastRootEvent{
			ToUid: fiberParams.Uid,
			Event: event,
		}
	}()

	return err
}

func (self *FiberDirector) UnregisterNode(uid string) error {
	self.fibersLock.Lock()
	defer self.fibersLock.Unlock()

	node, hasNode := self.fibers[uid]
	if !hasNode {
		return fmt.Errorf(
			"Node with id=%v doesn't exits",
			uid,
		)
	}

	node.fiber.Unrun()
	delete(self.fibers, uid)
	return nil
}
