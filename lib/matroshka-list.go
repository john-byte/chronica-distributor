package lib

import (
	"encoding/json"
	"errors"
)

const DefaultMatroshkaListCapacity uint32 = 1024

type MatroshkaListNode struct {
	value *Matroshka
	next  *MatroshkaListNode
}

type MatroshkaList struct {
	head           *MatroshkaListNode
	tail           *MatroshkaListNode
	count          uint32
	capacity       uint32
	packetCapacity uint32
}

func MakeMatroshkaList(capacity uint32, packetCapacity uint32) MatroshkaList {
	return MatroshkaList{
		head:           nil,
		tail:           nil,
		count:          0,
		capacity:       capacity,
		packetCapacity: packetCapacity,
	}
}

// - evt: Event | InternalEvent
func (self *MatroshkaList) Push(evt Event) error {
	if self.isFullImpl() {
		return errors.New("Matroshka list is full")
	}

	if self.tail == nil {
		matroshka := MakeMatroshka(self.packetCapacity)
		self.tail = &MatroshkaListNode{
			value: &matroshka,
			next:  nil,
		}
		self.head = self.tail
		self.count++
	}

	isLastMatroshkaFull := self.tail.value.IsFull()
	if isLastMatroshkaFull {
		matroshka := MakeMatroshka(self.packetCapacity)
		self.tail.next = &MatroshkaListNode{
			value: &matroshka,
			next:  nil,
		}
		self.tail = self.tail.next
		self.count++
	}

	return self.tail.value.Push(evt)
}

func (self *MatroshkaList) Pop() (res *Matroshka) {
	if self.head == nil {
		return nil
	}

	res = self.head.value

	self.head = self.head.next
	if self.head == nil {
		self.tail = nil
	}
	self.count--

	return res
}

func (self *MatroshkaList) Peek() *Matroshka {
	if self.head == nil {
		return nil
	}

	return self.head.value
}

func (self *MatroshkaList) isFullImpl() bool {
	isFullWithPackets := self.count == self.capacity
	isLastPacketFull := self.tail != nil && self.tail.value.IsFull()
	return isFullWithPackets && isLastPacketFull
}

func (self *MatroshkaList) IsFull() bool {
	return self.isFullImpl()
}

func (self *MatroshkaList) MarshalJSON() ([]byte, error) {
	var packedValue struct {
		Count          uint32       `json:"count"`
		Capacity       uint32       `json:"capacity"`
		PacketCapacity uint32       `json:"packetCapacity"`
		Buckets        []*Matroshka `json:"buckets"`
	}
	packedValue.Count = self.count
	packedValue.Capacity = self.capacity
	packedValue.PacketCapacity = self.packetCapacity
	packedValue.Buckets = make([]*Matroshka, 0)

	ptr := self.head
	for ptr != nil {
		packedValue.Buckets = append(packedValue.Buckets, ptr.value)
		ptr = ptr.next
	}

	result, err := json.Marshal(packedValue)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (self *MatroshkaList) UnmarshalJSON(raw []byte) error {
	var unpackedValue struct {
		Count          uint32       `json:"count"`
		Capacity       uint32       `json:"capacity"`
		PacketCapacity uint32       `json:"packetCapacity"`
		Buckets        []*Matroshka `json:"buckets"`
	}
	err := json.Unmarshal(raw, &unpackedValue)
	if err != nil {
		return err
	}

	self.count = unpackedValue.Count
	self.capacity = unpackedValue.Capacity
	self.packetCapacity = unpackedValue.PacketCapacity
	self.head = nil
	self.tail = nil

	for _, bucket := range unpackedValue.Buckets {
		if self.head == nil || self.tail == nil {
			self.head = &MatroshkaListNode{
				value: bucket,
				next:  nil,
			}
			self.tail = self.head
		} else {
			self.tail.next = &MatroshkaListNode{
				value: bucket,
				next:  nil,
			}
			self.tail = self.tail.next
		}
	}

	return nil
}
