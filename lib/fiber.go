package lib

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"

	fileutils "gitlab.com/john-byte/chronica-distributor/utils/file"
)

const externalSigCapacity = 256

type Fiber struct {
	node            Node
	events          *MatroshkaList
	internalEvents  *MatroshkaList
	externalSigChan chan interface{}
	pendingWorkChan chan struct{}
	closeSig        chan struct{}
	filePath        string
}

// Creates fiber
// - node: abstract node
// - eventsCapacity: maximum number of event buckets
// - packetCapacity: number of events in bucket
func MakeFiber(
	node Node,
	eventsCapacity uint32,
	packetCapacity uint32,
	filePath string,
) (*Fiber, error) {
	res := Fiber{
		node:            node,
		externalSigChan: make(chan interface{}, externalSigCapacity),
		pendingWorkChan: make(chan struct{}, 4),
		closeSig:        make(chan struct{}, 2),
		filePath:        filePath,
	}

	if len(filePath) > 0 {
		err := res.initEventsFromFileImpl()
		if err != nil {
			if errors.Is(err, os.ErrNotExist) {
				res.initDefaultEvents(eventsCapacity, packetCapacity)
			} else {
				return nil, err
			}
		}
	} else {
		res.initDefaultEvents(eventsCapacity, packetCapacity)
	}

	return &res, nil
}

func (self *Fiber) sendEventsImpl(evtsPacket *Matroshka, evtsQueue *MatroshkaList) bool {
	err := self.node.Send(evtsPacket)
	if err == nil {
		evtsQueue.Pop()
		self.pendingWorkChan <- struct{}{}
		return true
	} else {
		fmt.Printf("[CHRONICA] Err => %s\n", err.Error())
	}

	return false
}

// Starts fiber
func (self *Fiber) Run() {
	go func() {
		for {
			var externalSig interface{} = nil
			select {
			case <-self.closeSig:
				return
			case externalSig = <-self.externalSigChan:
			case <-self.pendingWorkChan:
			}
			for len(self.pendingWorkChan) > 0 {
				<-self.pendingWorkChan
			}

			queuesChanged := false

			if v, ok := externalSig.(Event); ok {
				if self.events.IsFull() {
					self.events.Pop()
				}

				self.events.Push(v)
				queuesChanged = true
			}

			if v, ok := externalSig.(Event); ok && len(v.TrustToken) > 0 {
				if self.internalEvents.IsFull() {
					self.internalEvents.Pop()
				}

				self.internalEvents.Push(v)
				queuesChanged = true
			}

			var internalPacket *Matroshka
			var packet *Matroshka

			internalPacket = self.internalEvents.Peek()
			if internalPacket != nil {
				queueChangedFromSend := self.sendEventsImpl(internalPacket, self.internalEvents)
				queuesChanged = queuesChanged || queueChangedFromSend
				goto flushQueues
			}

			packet = self.events.Peek()
			if packet != nil {
				queueChangedFromSend := self.sendEventsImpl(packet, self.events)
				queuesChanged = queuesChanged || queueChangedFromSend
			}

		flushQueues:
			{
				if len(self.filePath) > 0 && queuesChanged {
					err := self.saveEventsImpl()
					if err != nil {
						fmt.Printf("[CHRONICA] Err => %s\n", err.Error())
					}
				}
			}
		}
	}()
}

func (self *Fiber) Unrun() {
	err := self.saveEventsImpl()
	if err != nil {
		fmt.Printf("[CHRONICA] Err => %s\n", err.Error())
	}

	self.closeSig <- struct{}{}
	close(self.closeSig)
	close(self.externalSigChan)
	close(self.pendingWorkChan)
}

// Acknowledges for node's readiness.
// to flush fiber events into node
func (self *Fiber) AckNodeConnected() {
	self.externalSigChan <- struct{}{}
}

// Keeps event in internal queue.
// Events are then flushed into node in FIFO order
// - evt: event object
func (self *Fiber) SendEvent(evt Event) {
	self.externalSigChan <- evt
}

// Keeps root event (from Chronica system itself) in internal queue.
// Events are then flushed into node in FIFO order
// - evt: event object
func (self *Fiber) SendRootEvent(evt Event) {
	self.externalSigChan <- evt
}

func (self *Fiber) saveEventsImpl() error {
	var packedState struct {
		Events         *MatroshkaList `json:"events"`
		InternalEvents *MatroshkaList `json:"internalEvents"`
	}
	packedState.Events = self.events
	packedState.InternalEvents = self.internalEvents

	jsonText, err := json.Marshal(packedState)
	if err != nil {
		return err
	}
	err = fileutils.WriteFileWTimeout(
		self.filePath,
		jsonText,
		0666,
		3000,
	)
	return err
}

func (self *Fiber) initEventsFromFileImpl() error {
	file, err := fileutils.ReadFileWTimeout(
		self.filePath,
		3000,
	)
	if err != nil {
		return err
	}

	var unpackedState struct {
		Events         *MatroshkaList `json:"events"`
		InternalEvents *MatroshkaList `json:"internalEvents"`
	}
	err = json.Unmarshal(file, &unpackedState)
	if err != nil {
		return err
	}

	self.events = unpackedState.Events
	self.internalEvents = unpackedState.InternalEvents
	return nil
}

func (self *Fiber) initDefaultEvents(
	eventsCapacity uint32,
	packetCapacity uint32,
) {
	peerEventsList := MakeMatroshkaList(eventsCapacity, packetCapacity)
	internalEventsList := MakeMatroshkaList(256, 4)
	self.events = &peerEventsList
	self.internalEvents = &internalEventsList
}
