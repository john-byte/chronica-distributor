package lib

import (
	"encoding/json"
	"errors"
)

const DefaultMatroshkaCapacity uint32 = 32

type Matroshka struct {
	buffer   []*Event
	head     uint32
	capacity uint32
}

func MakeMatroshka(capacity uint32) Matroshka {
	return Matroshka{
		buffer:   make([]*Event, capacity),
		head:     0,
		capacity: capacity,
	}
}

func (self *Matroshka) Push(evt Event) error {
	if self.head >= self.capacity {
		return errors.New("Matroshka is full")
	}

	self.buffer[self.head] = &evt
	self.head++

	return nil
}

func (self *Matroshka) Get(id uint32) *Event {
	return self.buffer[id]
}

func (self *Matroshka) GetCapacity() uint32 {
	return self.capacity
}

func (self *Matroshka) GetCount() uint32 {
	return self.head
}

func (self *Matroshka) IsFull() bool {
	return self.head >= self.capacity
}

func (self *Matroshka) MarshalJSON() ([]byte, error) {
	var result struct {
		Capacity uint32   `json:"capacity"`
		Events   []*Event `json:"events"`
	}
	result.Capacity = self.capacity
	result.Events = make([]*Event, 0)

	for _, evt := range self.buffer {
		if evt == nil {
			break
		}

		result.Events = append(result.Events, evt)
	}

	rawResult, err := json.Marshal(result)
	if err != nil {
		return nil, err
	}

	return rawResult, nil
}

func (self *Matroshka) UnmarshalJSON(raw []byte) error {
	var unpackedStruct struct {
		Capacity uint32   `json:"capacity"`
		Events   []*Event `json:"events"`
	}

	err := json.Unmarshal(raw, &unpackedStruct)
	if err != nil {
		return err
	}

	buff := make([]*Event, unpackedStruct.Capacity)
	for i, evt := range unpackedStruct.Events {
		buff[i] = evt
	}

	self.buffer = buff
	self.capacity = unpackedStruct.Capacity
	self.head = uint32(len(self.buffer))
	return nil
}
