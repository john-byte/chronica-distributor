package lib

// Abstract interface for thing that commits events from internal queue
type Node interface {
	Send(events *Matroshka) error
}
