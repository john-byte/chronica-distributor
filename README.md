## How to install package
`go get gitlab.com/john-byte/chronica-distributor`

## Which problems this package solves
This is simple event/message distributor for applications with server. Its initial goal
is focused on how p2p-instances can replicate and filter data for their storages, but it can be used in wider set of cases involving synchronization of data between applications/servers.

For initial release only fixed set of nodes are supported.

## Chronica based architecture
![Chronica architecture](/chronica-architecture.svg)

## How events are stored
![Fiber internal data structure](/chronica-data-structure.svg)

When designing this data-structure, author came with static, dynamic arrays and plain linked lists. Array elements can be accessed in constant time, but they require big enough capacity
through whole application lifecycle to store realistic amount of events. That's not good for scaling amount of nodes. Dynamic arrays are not much better either: their capacity grows/shrinks exponentially, thus can cause significant delays on inserting events on buffer fullfillment. Linked-lists give stable amount of time for insertion/deletion of events,
but can't be optimized for faster memory access in cache, so they're generally slower for 
serialization of packet of events.

Then intermediate comes: linked-list with elements not for individual items, but for fixed-sized arrays of them.

## How events are flushed to node
1. Event is pushed into fiber queue;
2. Events from top of that queue are tried to be sent in packet;
3. If sending is succeed, then the packet is removed from queue. Otherwise is keeped;
4. If sending is succeed, then another packet of events is going to be flushed; 

## How to use Chronica
1. Install this package;
2. For your server entity implement Node interface that is endpoint for incoming events to other registered `Node`s;
3. Initialize `FiberDirector` with fixed set of named `Node`s and bucket size, maximum buckets count;
4. Run initialized `FiberDirector`;
5. Your server endpoint responsible for events book-keeping, should `SendSignal` to instantiated `FiberDirector`;

