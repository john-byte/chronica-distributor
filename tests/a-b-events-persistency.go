package tests

import (
	"fmt"
	"path"
	"time"

	"gitlab.com/john-byte/chronica-distributor/lib"
)

func ABEventsPersistency() {
	fmt.Print("\n\n[TEST_BEGIN] Events persistency\n")

	// [BEGIN] Prepare
	aliceUid := "alice"
	bobUid := "bob"
	alice := MakeMockNode(aliceUid)
	bob := MakeMockNode(bobUid)

	fiberParams := []lib.FiberParams{
		{
			Uid:  aliceUid,
			Node: &alice,
		},
		{
			Uid:  bobUid,
			Node: &bob,
		},
	}
	fiberManager, err := lib.MakeFiberDirector(
		fiberParams,
		MockEventsCapacity,
		MockPacketCapacity,
		MockTrustKey,
		path.Join("tests", "mock-data"),
	)
	if err != nil {
		panic(err)
	}
	// [END] Prepare

	// [BEGIN] Main setup
	fiberManager.Run()

	fiberManager.SendSignal(
		lib.SignalNodeConnected{
			Uid: aliceUid,
		},
	)
	fiberManager.SendSignal(
		lib.SignalNodeConnected{
			Uid: bobUid,
		},
	)

	fmt.Println("Bypassing Alice & Bob...")
	for !alice.HasEnoughEvents(0) || !bob.HasEnoughEvents(0) {
	}
	// [END] Main setup

	// [BEGIN] Send some events
	fiberManager.SendSignal(
		lib.SignalNewEvent{
			FromUid: aliceUid,
			Event: lib.Event{
				Action:  "example",
				Payload: "A1",
			},
		},
	)
	fiberManager.SendSignal(
		lib.SignalNewEvent{
			FromUid: aliceUid,
			Event: lib.Event{
				Action:  "example",
				Payload: "A2",
			},
		},
	)
	fiberManager.SendSignal(
		lib.SignalNewEvent{
			FromUid: bobUid,
			Event: lib.Event{
				Action:  "example",
				Payload: "B1",
			},
		},
	)
	fiberManager.SendSignal(
		lib.SignalNewEvent{
			FromUid: bobUid,
			Event: lib.Event{
				Action:  "example",
				Payload: "B2",
			},
		},
	)

	fmt.Println("Bypass unreleased events to Alice & Bob...")
	for !alice.HasEnoughEvents(0) || !bob.HasEnoughEvents(0) {
	}
	ShowNodeReceivedEvents(&alice)
	ShowNodeReceivedEvents(&bob)
	// [END] Send some events

	time.Sleep(time.Second * 3)

	// [BEGIN] Reload new fiber director & show events
	fiberManager.Unrun()
	fiberManager, err = lib.MakeFiberDirector(
		fiberParams,
		MockEventsCapacity,
		MockPacketCapacity,
		MockTrustKey,
		path.Join("tests", "mock-data"),
	)
	if err != nil {
		panic(err)
	}

	fiberManager.Run()
	alice.TurnOn()
	bob.TurnOn()

	fiberManager.SendSignal(
		lib.SignalNodeConnected{
			Uid: aliceUid,
		},
	)
	fiberManager.SendSignal(
		lib.SignalNodeConnected{
			Uid: bobUid,
		},
	)

	fmt.Println("Check previously stored & released events to Alice & Bob...")
	for !alice.HasEnoughEvents(2) || !bob.HasEnoughEvents(2) {
	}
	ShowNodeReceivedEvents(&alice)
	ShowNodeReceivedEvents(&bob)
	// [END] Reload new fiber director & show events

	fmt.Print("[TEST_END] Events persistency\n\n")
}
