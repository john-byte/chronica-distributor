package tests

import (
	"fmt"
	"gitlab.com/john-byte/chronica-distributor/lib"
)

func ABPubSubSimple() {
	fmt.Print("\n\n[TEST_BEGIN] Connect new node to orchestra\n")

	// [BEGIN] Prepare
	aliceUid := "alice"
	bobUid := "bob"
	alice := MakeMockNode(aliceUid)
	bob := MakeMockNode(bobUid)

	fiberParams := []lib.FiberParams{
		{
			Uid:  aliceUid,
			Node: &alice,
		},
		{
			Uid:  bobUid,
			Node: &bob,
		},
	}
	fiberManager, _ := lib.MakeFiberDirector(
		fiberParams,
		MockEventsCapacity,
		MockPacketCapacity,
		MockTrustKey,
		"",
	)
	// [END] Prepare

	// [BEGIN] Main setup
	fiberManager.Run()
	alice.TurnOn()
	bob.TurnOn()

	fiberManager.SendSignal(
		lib.SignalNodeConnected{
			Uid: aliceUid,
		},
	)
	fiberManager.SendSignal(
		lib.SignalNodeConnected{
			Uid: bobUid,
		},
	)

	fmt.Println("Bypassing Alice & Bob...")
	for !alice.HasEnoughEvents(0) || !bob.HasEnoughEvents(0) {
	}
	// [END] Main setup

	// [BEGIN] Setup pubsubbed node
	charlieUid := "charlie"
	charlie := MakeMockNode(charlieUid)
	charlie.TurnOn()
	fiberManager.RegisterNode(
		lib.FiberParams{
			Uid:  charlieUid,
			Node: &charlie,
		},
	)
	// [END] Setup pubsubbed node

	// [BEGIN] Assertions

	fmt.Println("Bypassing Charlie...")
	for !charlie.HasEnoughEvents(1) {
	}
	ShowNodeReceivedFullInternalEvents(&charlie)

	// [END] Assertions

	fmt.Print("[TEST_END] Connect new node to orchestra\n\n")
}
