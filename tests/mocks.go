package tests

const MockPacketCapacity uint32 = 4
const MockEventsCapacity uint32 = 2
const MockTrustKey string = "example-key"
