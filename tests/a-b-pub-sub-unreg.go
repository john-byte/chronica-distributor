package tests

import (
	"fmt"
	"gitlab.com/john-byte/chronica-distributor/lib"
)

func ABPubSubWithUnregister() {
	fmt.Print("\n\n[TEST_BEGIN] Unregister node from orchestra\n")

	// [BEGIN] Prepare
	aliceUid := "alice"
	bobUid := "bob"
	charlieUid := "charlie"
	alice := MakeMockNode(aliceUid)
	bob := MakeMockNode(bobUid)
	charlie := MakeMockNode(charlieUid)

	fiberParams := []lib.FiberParams{
		{
			Uid:  aliceUid,
			Node: &alice,
		},
		{
			Uid:  bobUid,
			Node: &bob,
		},
		{
			Uid:  charlieUid,
			Node: &charlie,
		},
	}
	fiberManager, _ := lib.MakeFiberDirector(
		fiberParams,
		MockEventsCapacity,
		MockPacketCapacity,
		MockTrustKey,
		"",
	)
	// [END] Prepare

	// [BEGIN] Main setup
	fiberManager.Run()
	alice.TurnOn()
	bob.TurnOn()
	charlie.TurnOn()

	fiberManager.SendSignal(
		lib.SignalNodeConnected{
			Uid: aliceUid,
		},
	)
	fiberManager.SendSignal(
		lib.SignalNodeConnected{
			Uid: bobUid,
		},
	)
	fiberManager.SendSignal(
		lib.SignalNodeConnected{
			Uid: charlieUid,
		},
	)

	fmt.Println("Bypassing Alice & Bob & Charlie...")
	for !alice.HasEnoughEvents(0) || !bob.HasEnoughEvents(0) || !charlie.HasEnoughEvents(0) {
	}
	// [END] Main setup

	// [BEGIN] Send some events
	fiberManager.SendSignal(
		lib.SignalNewEvent{
			FromUid: aliceUid,
			Event: lib.Event{
				Action:  "alice-evt",
				Payload: "A1",
			},
		},
	)
	fiberManager.SendSignal(
		lib.SignalNewEvent{
			FromUid: bobUid,
			Event: lib.Event{
				Action:  "bob-evt",
				Payload: "B1",
			},
		},
	)
	fiberManager.SendSignal(
		lib.SignalNewEvent{
			FromUid: charlieUid,
			Event: lib.Event{
				Action:  "charlie-evt",
				Payload: "C1",
			},
		},
	)
	fmt.Println("Receiving events for Alice & Bob & Charlie...")
	for !alice.HasEnoughEvents(2) || !bob.HasEnoughEvents(2) || !charlie.HasEnoughEvents(2) {
	}
	ShowNodeReceivedEvents(&alice)
	ShowNodeReceivedEvents(&bob)
	ShowNodeReceivedEvents(&charlie)
	// [END] Send some events

	// [BEGIN] Turn off Bob
	bob.TurnOff()
	fiberManager.UnregisterNode(bobUid)
	fiberManager.SendSignal(
		lib.SignalNewEvent{
			FromUid: aliceUid,
			Event: lib.Event{
				Action:  "alice-evt",
				Payload: "A2",
			},
		},
	)
	fiberManager.SendSignal(
		lib.SignalNewEvent{
			FromUid: bobUid,
			Event: lib.Event{
				Action:  "bob-evt",
				Payload: "B2",
			},
		},
	)
	fiberManager.SendSignal(
		lib.SignalNewEvent{
			FromUid: charlieUid,
			Event: lib.Event{
				Action:  "charlie-evt",
				Payload: "C2",
			},
		},
	)
	fmt.Println("Receiving events for Alice & Charlie...")
	for !alice.HasEnoughEvents(3) || !charlie.HasEnoughEvents(3) {
	}
	ShowNodeReceivedEvents(&alice)
	ShowNodeReceivedEvents(&charlie)
	// [END] Turn off Bob

	fmt.Print("\n\n[TEST_END] Unregister node from orchestra\n")
}
