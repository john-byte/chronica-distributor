package tests

import (
	"errors"
	"fmt"
	"gitlab.com/john-byte/chronica-distributor/lib"
)

type MockNode struct {
	url       string
	turnedOn  bool
	resBuffer []*lib.Event
}

func MakeMockNode(url string) MockNode {
	return MockNode{
		url:       url,
		turnedOn:  false,
		resBuffer: make([]*lib.Event, 0),
	}
}

func (self *MockNode) TurnOn() {
	self.turnedOn = true
}

func (self *MockNode) TurnOff() {
	self.turnedOn = false
}

func (self *MockNode) GetEvents() []*lib.Event {
	return self.resBuffer
}

// Interface method
func (self *MockNode) Send(events *lib.Matroshka) error {
	if !self.turnedOn {
		return errors.New(
			fmt.Sprintf("Node \"%v\" is turned off", self.url),
		)
	}

	var i uint32
	for ; i < events.GetCount(); i += 1 {
		buffLen := len(self.resBuffer)
		currEvt := events.Get(i)
		if currEvt != nil && (buffLen == 0 || currEvt != self.resBuffer[buffLen-1]) {
			self.resBuffer = append(self.resBuffer, currEvt)
		}
	}

	return nil
}

func (self *MockNode) HasEnoughEvents(expEventsCount uint32) bool {
	return len(self.resBuffer) == int(expEventsCount)
}
