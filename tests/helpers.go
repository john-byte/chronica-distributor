package tests

import (
	"fmt"
	"strings"
)

func ShowNodeReceivedEvents(node *MockNode) {
	nodeEvents := node.GetEvents()
	nodeEvtsStrs := make([]string, 0)
	for _, evt := range nodeEvents {
		if evt == nil {
			nodeEvtsStrs = append(
				nodeEvtsStrs,
				"?",
			)
		} else {
			evtRes := evt
			nodeEvtsStrs = append(
				nodeEvtsStrs,
				evtRes.Payload.(string),
			)
		}
	}
	fmt.Printf("\"%s\" received events => [%s]\n", node.url, strings.Join(nodeEvtsStrs, ", "))
}

func ShowNodeReceivedFullInternalEvents(node *MockNode) {
	nodeEvents := node.GetEvents()
	nodeEvtsStrs := make([]string, 0)
	for _, evt := range nodeEvents {
		if evt == nil {
			nodeEvtsStrs = append(
				nodeEvtsStrs,
				"?",
			)
		} else {
			evtRes := evt
			nodeEvtsStrs = append(
				nodeEvtsStrs,
				fmt.Sprintf(
					"action: %v, trustToken: %v, payload: %v",
					evtRes.Action,
					evtRes.TrustToken,
					evtRes.Payload,
				),
			)
		}
	}
	fmt.Printf("\"%s\" received events => [%s]\n", node.url, strings.Join(nodeEvtsStrs, ",\n"))
}
