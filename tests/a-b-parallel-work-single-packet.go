package tests

import (
	"fmt"
	"gitlab.com/john-byte/chronica-distributor/lib"
)

func ABParallelWorkSinglePacket() {
	fmt.Print("\n\n[TEST_BEGIN] Nodes sending events in parallel\n")

	// [BEGIN] Prepare
	aliceUid := "alice"
	bobUid := "bob"
	alice := MakeMockNode(aliceUid)
	bob := MakeMockNode(bobUid)

	fiberParams := []lib.FiberParams{
		{
			Uid:  aliceUid,
			Node: &alice,
		},
		{
			Uid:  bobUid,
			Node: &bob,
		},
	}
	fiberManager, _ := lib.MakeFiberDirector(
		fiberParams,
		MockEventsCapacity,
		MockPacketCapacity,
		"",
		"",
	)
	// [END] Prepare

	// [BEGIN] Main setup
	fiberManager.Run()
	alice.TurnOn()
	bob.TurnOn()

	fiberManager.SendSignal(
		lib.SignalNodeConnected{
			Uid: aliceUid,
		},
	)
	fiberManager.SendSignal(
		lib.SignalNodeConnected{
			Uid: bobUid,
		},
	)

	for i := 0; i < int(MockPacketCapacity); i += 1 {
		fiberManager.SendSignal(
			lib.SignalNewEvent{
				FromUid: aliceUid,
				Event: lib.Event{
					Action:  "example",
					Payload: fmt.Sprintf("A%d", i+1),
				},
			},
		)
		fiberManager.SendSignal(
			lib.SignalNewEvent{
				FromUid: bobUid,
				Event: lib.Event{
					Action:  "example",
					Payload: fmt.Sprintf("B%d", i+1),
				},
			},
		)
	}

	// [END] Main setup

	// [BEGIN] Assertions
	fmt.Printf("Waiting for events on Alice & Bob\n")
	for !alice.HasEnoughEvents(MockPacketCapacity) || !bob.HasEnoughEvents(MockPacketCapacity) {
	}

	ShowNodeReceivedEvents(&alice)
	ShowNodeReceivedEvents(&bob)
	// [END] Assertions

	fmt.Print("[TEST_END] Nodes sending events in parallel\n\n")
}
